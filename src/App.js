import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import WelcomePage from './Auth/WelcomPage';


function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={WelcomePage} />
            </Switch>
        </Router>
    );
}

export default App;
